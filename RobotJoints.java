
/**
* This class will represent a pose and reimplement UR's pose functions

* @author  Paul Weidinger
*/

import java.io.File;
import java.io.FileReader;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Vector;


public class RobotJoints {
	private static final double HALF_PI = Math.PI*0.5;
	protected static double[] a = null;
	protected static double[] d = null;
	protected static double[] alpha = null;
	protected static double[] theta = null;
	private double[] jointAngles;
 

	public RobotJoints(double[] jointAngles) {
		this.jointAngles = jointAngles;
		setDHParameters();
	}

	protected static void setDHParameters() {
		if (a != null && d != null && alpha != null && theta != null) {
			//Already set.
			return;
		}
		double[] a = {0,0,0,0,0,0};
		double[] d = {0,0,0,0,0,0};
		double[] alpha = {HALF_PI, 0, 0, HALF_PI, -HALF_PI, 0};
		double[] theta = {0,0,0,0,0,0};
		//Add the correct DH Parameters. ( From https://www.universal-robots.com/how-tos-and-faqs/faq/ur-faq/parameters-for-calculations-of-kinematics-and-dynamics-45257/ )
		String series = "CB3";//YourURCapsInstallationNodeContribution.apiProvider.getSystemAPI().getRobotModel().getRobotSeries().name();
		// I am testing and comparing with a UR10 simulator
		String model = "UR10";//YourURCapsInstallationNodeContribution.apiProvider.getSystemAPI().getRobotModel().getRobotType().name();

		if (series.equals("E_SERIES")) {
			if (model.equals("UR10")) {
				a[1] = -0.6127;
				a[2] = -0.57155;
				d[0] = 0.1807;
				d[3] = 0.17415;
				d[4] = 0.11985;
				d[5] = 0.11655;
			} else if (model.equals("UR5")) {
				a[1] = -0.425;
				a[2] = -0.3922;
				d[0] = 0.1625;
				d[3] = 0.1333;
				d[4] = 0.0997;
				d[5] = 0.0996;
			} else if (model.equals("UR3")) {
				a[1] = -0.24355;
				a[2] = -0.2132;
				d[0] = 0.15185;
				d[3] = 0.13105;
				d[4] = 0.08535;
				d[5] = 0.0921;
			} else {
				System.out.println("ERROR while creating RobotJoints: Series given is not UR10, UR5 or UR3: " + series);
				return;
			}
		} else if (series.equals("CB3")) {
			if (model.equals("UR10")) {
				a[1] = -0.612;
				a[2] = -0.5723;
				d[0] = 0.1273;
				d[3] = 0.163941;
				d[4] = 0.1157;
				d[5] = 0.0922;
			} else if (model.equals("UR5")) {
				a[1] = -0.425;
				a[2] = -0.39225;
				d[0] = 0.089159;
				d[3] = 0.10915;
				d[4] = 0.09465;
				d[5] = 0.0823;
			} else if (model.equals("UR3")) {
				a[1] = -0.24365;
				a[2] = -0.21325;
				d[0] = 0.1519;
				d[3] = 0.11235;
				d[4] = 0.08535;
				d[5] = 0.0819;
			} else {
				System.out.println("ERROR while creating RobotJoints: Series given is not UR10, UR5 or UR3: " + series);
				return;
			}
		} else {
			System.out.println("ERROR while creating RobotJoints: Series given is not E_SERIES or CB3: " + series);
			return;
		}

		//if ( YourURCapsInstallationNodeContribution.apiProvider.getSystemAPI().getRobotSimulation().isRealRobot() ) {
			// Add DH Calibration Configuration 
			File calibConfig = new File("/root/.urcontrol/calibration.conf");
			if (calibConfig.exists()) {
				String fileString = readStringFromFile(calibConfig, Integer.MAX_VALUE, 0, "joint_checksum");
				String[] fileLines = fileString.split("\n");
				double[] thetaDelta = parseCoordinatesFromString(fileLines[1].split("=")[1]); // Second line in the file is Theta
				for (int i = 0; i < theta.length; i++) {
					theta[i] = theta[i] + thetaDelta[i];
				}
				double[] aDelta = parseCoordinatesFromString(fileLines[2].split("=")[1]); // Third line in the file is a
				for (int i = 0; i < a.length; i++) {
					a[i] = a[i] + aDelta[i];
				}
				double[] dDelta = parseCoordinatesFromString(fileLines[3].split("=")[1]); // Forth line in the file is d
				for (int i = 0; i < d.length; i++) {
					d[i] = d[i] + dDelta[i];
				}
				double[] alphaDelta = parseCoordinatesFromString(fileLines[4].split("=")[1]); // Fifth line in the file is alpha
				for (int i = 0; i < alpha.length; i++) {
					alpha[i] = alpha[i] + alphaDelta[i];
				}
			// }
		}
		RobotJoints.a = a;
		RobotJoints.d = d;
		RobotJoints.alpha = alpha;
		RobotJoints.theta = theta;
	}

	protected static double[][] getDHMatrix(double[] jointAngles) {
		return getDHMatrix(0, jointAngles);
	}

	protected static double[][] getDHMatrix(int joint, double jointAngles) {
		return getDHMatrix(joint, new double[]{jointAngles});
	}

	// Joint Angles include the Theta calibration DH Parameters.
	// This can be used to get the pose of the elbow for example.
	protected static double[][] getDHMatrix(int startingJoint, double[] jointAngle) {
		double[] jointAngles = jointAngle.clone();
		setDHParameters();
		double[][] t = null;
		for (int i = startingJoint; i < (jointAngles.length); i++) {
			jointAngles[i] = jointAngles[i]+RobotJoints.theta[i];
			double[][] z = { { Math.cos(jointAngles[i]),	-Math.sin(jointAngles[i]),	0,	0		},
							 { Math.sin(jointAngles[i]),	Math.cos(jointAngles[i]), 	0,	0		},
							 { 0,							0, 							1,	RobotJoints.d[i] 	},
							 { 0,							0, 							0,	1	 	},	};
			
			double[][] x = { { 1,	0,				0,					RobotJoints.a[i]	},
							 { 0,	Math.cos(RobotJoints.alpha[i]),	-Math.sin(RobotJoints.alpha[i]), 	0		},
							 { 0,	Math.sin(RobotJoints.alpha[i]),	Math.cos(RobotJoints.alpha[i]), 	0		},
							 { 0,	0, 				0,					1	 	},	};
			if (t == null) {
				t = Matrix.multiply(z, x);
			} else {
				t = Matrix.multiply(t, Matrix.multiply(z, x));
			}
		}
		return t;
	}

	public double[] getForwardKinematics() {
		return getForwardKinematics(this.jointAngles);
	}

	public static double[] getForwardKinematics(double[] jointAngles) {
		RobotPose pose = new RobotPose(getDHMatrix(jointAngles));
		return pose.toDoubleArray();
	}
	
	public double[] getForwardKinematicsWithTCP(double[] tcpPose) {
		return getForwardKinematics(this.jointAngles,tcpPose);
	}

	public static double[] getForwardKinematics(double[] jointAngles, double[] tcpPose) {
		if (jointAngles.length != 6) {
			// Need all 6 joints for this.
			return null;
		}
		return RobotPose.pose_trans(new RobotPose(getDHMatrix(jointAngles)).toDoubleArray(), tcpPose);
	}

	public RobotJoints transformBaseJointPosition(RobotPose newPose) {
		return (RobotJoints.transformBaseJointPosition(this, newPose));
	}

	public static RobotJoints transformBaseJointPosition(final RobotJoints oldJP, final RobotPose newPose) throws IllegalStateException {
		RobotJoints newJP = null;
		RobotPose oldPose = new RobotPose(getForwardKinematics(oldJP.toDoubleArray()));
		System.out.println(oldPose.asString());
		double[] oldDoubles = oldPose.toDoubleArray();
		double oldPoseAngle = Math.atan(oldDoubles[1]/oldDoubles[0]); // Angle of oldPose from X Axis in the XY Plane
		System.out.println("Old Angle: "+oldPoseAngle);
		
		double[] newDoubles = newPose.toDoubleArray();
		double newPoseAngle = Math.atan(newDoubles[1]/newDoubles[0]); // Angle of oldPose from X Axis in the XY Plane
		System.out.println("New Angle: "+newPoseAngle);
		
		if (oldDoubles[0] < 0 && newDoubles[0] >= 0) {
			if (oldDoubles[1] < 0) {
				oldPoseAngle = Math.PI+oldPoseAngle;
			} else {
				oldPoseAngle = -Math.PI+oldPoseAngle;
			}
		} else if (newDoubles[0] < 0 && oldDoubles[0] >= 0) {
			if (newDoubles[1] >= 0) {
				newPoseAngle = Math.PI+newPoseAngle;
			} else {
				newPoseAngle = -Math.PI+newPoseAngle;
			}
		}
		System.out.println("New Angle: "+newPoseAngle + " and Old Angle: "+ oldPoseAngle);
		oldDoubles = oldJP.toDoubleArray();
		//UR Base angle increases as pose angle from X Axis in the XY Plane decreases, so subtract angle difference.
		oldDoubles[0]+=(newPoseAngle-oldPoseAngle);
		RobotJoints newJPQnear =  new RobotJoints(oldDoubles);
		// System.out.println("Qnear to inverse: "+newJPQnear.asString());
		// try {
		// 	newJP = new RobotJoints(RobotPose.getInverseKinematics(newPose, newJPQnear.toDoubleArray())); // If the return is supposed to be a QNear, this step is not necessary.
		// } catch (Exception e){
		// 	e.printStackTrace();
			newJP = newJPQnear;
		// }
		// if (newJP == null) {
		// 	throw new IllegalStateException("Could not calculate a new JointPosition for the pose "+newPose.toString());
		// }
		return newJP;
	}
	
	public String asString() {
		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits(4);
		String str = "[ ";
		for (int i = 0; i < jointAngles.length; i++) {
			str += df.format(epsilon(jointAngles[i]));
			if (i < jointAngles.length - 1) str += ", ";
		}
		str += " ]";
		return str;
	}

	public double[] toDoubleArray() {
		return jointAngles.clone();
	}
	
	private double epsilon(double comp) {
		if (Math.abs(comp) < .001) return 0;
		else return comp;
	}

	protected static double[] parseCoordinatesFromString(String word) {
		double[] coord = new double[6];
		String[] parts = word.split("\\[")[1].split("\\]")[0].split(",");
		for (int i = 0; i < parts.length; i++) {
			coord[i] = Double.parseDouble(parts[i]);
		}
		return coord;
	}

	protected static String readStringFromFile(File file, int maxLengthOfString, long skipSymbols, String stopReadingAt) {
		FileReader fileReader = null;
		String fileHeader = "";

		try {
			fileReader = new FileReader(file);
			fileReader.skip(skipSymbols);
			char l = (char) fileReader.read();
			for (int j = 0; j < maxLengthOfString || l == -1; j++, l = (char) fileReader.read()) {
				fileHeader = fileHeader.concat(String.valueOf(l));
				if ( fileHeader.endsWith(stopReadingAt) ) {
					fileHeader = fileHeader.split(stopReadingAt)[0];
					break;
				}
			}
			fileReader.close();
			return fileHeader;
		}catch (Exception e) {
        	System.out.println("Got Exception "+e.getMessage()+" when reading from "+file.getAbsolutePath());
			try{ fileReader.close();
			} catch (Exception except) { }
		}
		return null;
	}

	public static void main(String[] args) {
		RobotJoints test = new RobotJoints(new double[] {HALF_PI,-HALF_PI,-HALF_PI,-HALF_PI,HALF_PI,0});
		RobotPose newPose = new RobotPose(new double[] {-0.17,0.69, 0.68,3.1415,0.000001,-0.000000001});
		RobotJoints test2 = RobotJoints.transformBaseJointPosition(test,newPose);

		System.out.println(test.asString() + " and " +test2.asString());

		System.out.println("Want: "+newPose.asString()+"  but got: "+new RobotPose(test2.getForwardKinematics()).asString());
	}
}
