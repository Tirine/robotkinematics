Robot kinematics and useful functions for UR. 
Additions to the work by: paul weidinger, pweids, https://gist.github.com/pweids 
Find his work in the UR Forum: 
https://forum.universal-robots.com/t/pose-trans-and-pose-inv-functions-in-urcap/1257/6