
/**
* This class will represent a pose and reimplement UR's pose functions

* @author  Paul Weidinger
*/

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;

public class RobotPose {
	Vector<Double> pose;

	public RobotPose(double[] pose) {
		this.pose = new Vector<Double>();
		for (int i = 0; i < pose.length; i++)
			this.pose.add(pose[i]);
		
	}
	
	public RobotPose(double[][] xformMat) {
		this.pose = new Vector<Double>();
		this.pose.add(xformMat[0][3]);
		this.pose.add(xformMat[1][3]);
		this.pose.add(xformMat[2][3]);
		AxisAngle rot = new AxisAngle(new double[][]{
				{xformMat[0][0], xformMat[0][1], xformMat[0][2]},
				{xformMat[1][0], xformMat[1][1], xformMat[1][2]},
				{xformMat[2][0], xformMat[2][1], xformMat[2][2]}
		});
		double[] rv = rot.getRotationVector();
		this.pose.add(rv[0]);
		this.pose.add(rv[1]);
		this.pose.add(rv[2]);
	}
	
	public void setRxRyRz(double[] rot) {
		setRxRyRz(rot[0], rot[1], rot[2]);
	}
	
	public void setRxRyRz(double rx, double ry, double rz) {
		pose.set(3, rx);
		pose.set(4, ry);
		pose.set(5, rz);
	}
	
	public void setXYZ(double[] pos) {
		setXYZ(pos[0], pos[1], pos[2]);
	}
	
	public void setXYZ(double x, double y, double z) {
		pose.set(0, x);
		pose.set(1, y);
		pose.set(2, z);
	}
	
	public double[] getRotationVector() {
		List<Double> sublist = pose.subList(3, 6);
		return convertListToPrimitives(sublist.toArray(new Double[3]));
		}
	
	private double[] convertListToPrimitives(Double[] list) {
		int len = list.length;
		double[] retArr = new double[len];
		for (int i = 0; i < len; i++)
			retArr[i] = list[i];
		
		return retArr;
	}
	
	public double[] getTranslationVector() {
		List<Double> sublist = pose.subList(0,3);
		return convertListToPrimitives(sublist.toArray(new Double[3]));
	}

	public double[] invert() {
		Matrix rotMat = new Matrix(getTransformMatrix());
		Matrix invMat = rotMat.invertTransformation();
		AxisAngle angle = new AxisAngle(invMat.subMatrix(0, 3, 0, 3));
		double[] rotVec = angle.getRotationVector();
		double[][] transMat = invMat.subMatrix(0, 3, 3, 4);
		
		double[] newPose = new double[]{
				transMat[0][0], transMat[1][0], transMat[2][0],
				rotVec[0], rotVec[1], rotVec[2]
		};
		
		return newPose;
	}
	
	public double[][] getTransformMatrix() {
		double[] trans = getTranslationVector();
		double[] rot = getRotationVector();
		
		AxisAngle angle = new AxisAngle(rot);
		double[][] rotMat = angle.getRotationMatrix();
		
		double[][] mat = new double[][] {
				{rotMat[0][0], rotMat[0][1], rotMat[0][2], trans[0]},
				{rotMat[1][0], rotMat[1][1], rotMat[1][2], trans[1]},
				{rotMat[2][0], rotMat[2][1], rotMat[2][2], trans[2]},
				{0, 0, 0, 1}
		};
		
		return mat;
	}

	public double[] getInverseKinematics(double[] qNear) {
		return getInverseKinematics(this, qNear);
	}

	public double[] getInverseKinematics(double[] tcp, double[] qNear) {
		RobotPose tcpPose = new RobotPose(new RobotPose(tcp).invert());
		return getInverseKinematics(this.pose_trans(tcpPose),qNear);
	}

	public static double[] getInverseKinematics(RobotPose pose, RobotPose tcp, double[] qNear) {
		tcp = new RobotPose(tcp.invert());
		return getInverseKinematics(pose.pose_trans(tcp),qNear);
	}

	//Should be improved.
	public static boolean isReachable(double[] pose) {
		// I am testing and comparing with a UR10 simulator
		String model = "UR10";//YourURCapsInstallationNodeContribution.apiProvider.getSystemAPI().getRobotModel().getRobotType().name();
		double maxDistance = 0.5;
		double d0 = 0.15;
		if (model.equals("UR10")) {
			maxDistance = 1.3;
			d0 = 0.18;
		} else if (model.equals("UR5")) {
			maxDistance = 0.85;
			d0 = 0.16;
		} else if (model.equals("UR16")) {
			maxDistance = 0.9;
			d0 = 0.16; // This is a guess...
		}
		if (distance(pose, new double[]{0,0,d0/2,0,0,0}) < d0/2 || distance(pose, new double[]{0,0,d0,0,0,0}) > maxDistance) {
			return false;
		}
		return true;
	}

	public boolean isReachable() {
		return isReachable(this.toDoubleArray());
	}

	//Pose should be TCP flange.
	// Problem that sometimes the wrist angles get stuck in a local minimum..
	public static double[] getInverseKinematics(RobotPose pose, double[] jointAngles) {
		// TODO : Check if reachable?		
		if (!pose.isReachable()) {
			throw new IllegalArgumentException("Pose not reachable:"+pose.asString());
		}
		double[] qNear = jointAngles;
		double[] qNearGoodEnough = null;

		RobotJoints.setDHParameters();
		double deltaValue = 0.00314159265;
		double[] qNearDelta = {0,0,0,0,0,0};
		
		for (int i = 0; i < qNear.length; i++) {
			if (qNear[i] == 0) {
				qNearDelta[i] = deltaValue;
			} else {
				qNearDelta[i] = deltaValue*(-qNear[i]/Math.abs(qNear[i])); // If qNear is neg, startAngle is positive
			}
		}
		RobotPose newPosition = new RobotPose(RobotJoints.getDHMatrix(qNear));
		double[] newError = {0,0,0,0,0,0};
		double minDelta = 0.01;
		double[] error = {0,0,0,0,0,0};
		double[] rotVec1;
		double[] rotVec2;
		int j = 1;
		int n = 1;
		double modifier = 1;
		for (j = 1;!pose.epsilonEquals(newPosition.toDoubleArray(), 0.0004, 0.014); j++) {
			if (j == 10000) {
				if (pose.epsilonEquals(newPosition.toDoubleArray(), 0.001, 0.03)) {
					break;
				}
				if (qNearGoodEnough != null) {
					qNear = qNearGoodEnough;
					break;
				}
				// throw new IllegalArgumentException("Possibly unreachable pose, timeout on calc: " + pose.asString()+"\nGot to "+Arrays.toString(qNear) +" with pose: "+new RobotPose(RobotJoints.getDHMatrix(qNear)).asString());
				break;
			}
			if (pose.epsilonEquals(newPosition.toDoubleArray(), 0.4/n, 1/n)) {
				n=n+2;
			}
			minDelta =  (0.00001+0.0001/n+0.001/j);
			for (int i = 0; i < 6; i++) {
				if (i > 2 ) {
					modifier = 0.2;
				} else {						
					modifier = 5;
				}
				rotVec1 = newPosition.getRotationVector();
				rotVec2 = pose.getRotationVector();
				error[i] = (1/modifier)*distance(rotVec1, rotVec2)+((0.1/n)+(0.2/j))*(Math.abs(rotVec1[1]-rotVec2[1])+Math.abs(rotVec1[2]-rotVec2[2])+Math.abs(rotVec1[0]-rotVec2[0]));
				error[i] += modifier*pose.pose_dist(newPosition)*10;
				
				if (Math.abs(qNearDelta[i]) < minDelta) {
					qNearDelta[i] = (error[i]+minDelta)*(qNearDelta[i]/Math.abs(qNearDelta[i]));
				}
				qNear[i] += qNearDelta[i];
				newPosition = new RobotPose(RobotJoints.getDHMatrix(qNear));

				rotVec1 = newPosition.getRotationVector();
				rotVec2 = pose.getRotationVector();
				newError[i] = (1/modifier)*distance(rotVec1, rotVec2)+((0.1/n)+(0.2/j))*(Math.abs(rotVec1[1]-rotVec2[1])+Math.abs(rotVec1[2]-rotVec2[2])+Math.abs(rotVec1[0]-rotVec2[0]));					
				newError[i] += modifier*pose.pose_dist(newPosition)*10;

				if (newError[i] > error[i]) {
					qNearDelta[i] = -qNearDelta[i]*0.5;
				} else {
					qNearDelta[i] = qNearDelta[i]*1.1;
				}

				if (qNear[i] > Math.PI*2) {
					qNear[i] -= Math.PI*2; 
				} else if (qNear[i] < -Math.PI*2) {
					qNear[i] += Math.PI*2; 
				}
				if (pose.epsilonEquals(newPosition.toDoubleArray(), 0.0004, 0.014)) {
					break;
				}
				if (pose.epsilonEquals(newPosition.toDoubleArray(), 0.001, 0.03)) {
					qNearGoodEnough = qNear;
				}			
			}
			if (Math.IEEEremainder(j, 1000) == 0) {
				System.out.println("[DEBUG] "+"Cycles: "+j +" with qNearPose: " + newPosition.asString() + " and error: " + Arrays.toString(error));
			}
			
		}
		for (int i = 0; i < 6; i++) {
			double twoPi = Math.PI*2;
			if (qNear[i] > twoPi) {
				qNear[i] = Math.IEEEremainder(qNear[i], twoPi); 
			}
			if (qNear[i] < -twoPi) {
				qNear[i] = -Math.IEEEremainder(qNear[i], twoPi); 
			}
			if ((qNear[i]-jointAngles[i]) < -Math.PI) {
				qNear[i] += twoPi; 
			} else if ((qNear[i]-jointAngles[i]) > Math.PI) {
				qNear[i] -= twoPi; 
			}
		}
				System.out.println("[DEBUG] "+"Cycles: "+j +" with qNearPose: " + newPosition.asString() + " and error: " + Arrays.toString(error));
		return qNear;
		
	}



	// // My first try, with computing jacobian. It was just waaaay slower.... 
	public static double[] getInverseKinematicsJacob(RobotPose goalPose, double[] jointAngles) {
		double[] qNear = jointAngles.clone();
		double deltaH = 0.00001;
		double[][] jacobianDouble = new double[qNear.length][qNear.length];
		Matrix jacobian = new Matrix( new double[qNear.length][qNear.length] );
		double[][] transposedJacobianDouble = new double[qNear.length][qNear.length];
		Matrix transposedJacobian = new Matrix(jacobianDouble);;
		double[] deltaE = new double[goalPose.toDoubleArray().length];
		double[] goalPoseDouble = goalPose.toDoubleArray();
		double[][] invJacobian;
		double[] qNearPose = RobotJoints.getForwardKinematics(qNear);
		double[] qNearDelta = new double[qNear.length];
	
		int c = 1;
		double n = 1;
		long time = System.currentTimeMillis();
		while (!goalPose.epsilonEquals(qNearPose,  0.0008, 0.028)) {
			if (goalPose.epsilonEquals(qNearPose, 0.04/n, 0.7/n)) {
				n++;
				// System.out.println("[DEBUG] "+n);
			}
			
			jacobianDouble = computeJacobian(qNear, deltaH);
			jacobian = new Matrix(jacobianDouble);

			// double det = jacobian.determinant();
			// if (det == 0) {
				transposedJacobianDouble = jacobian.transpose();
				transposedJacobian = new Matrix(transposedJacobianDouble);
				// J+ = (JT*J)^(-1)*JT PseudoInverse
				invJacobian = Matrix.multiply( Matrix.invert( transposedJacobian.multiply(jacobianDouble) ), transposedJacobianDouble);
				// System.out.println("[DEBUG] "+"Had to use pseudoInv: "+det + "\n" + new Matrix(invJacobian).toString());
			// } else {
				// True inverse
				// invJacobian = jacobian.inverse();
			// }
			for (int i = 0; i < qNearPose.length; i++) {
				qNearDelta[i] = 0;

				for (int j = 0; j < qNearPose.length; j++) {
					deltaE[j] = goalPoseDouble[j]-qNearPose[j];
					qNearDelta[i] -= Math.abs(0.5+i-j)*invJacobian[j][i]*Math.sqrt(Math.abs(deltaE[j]))*deltaE[j]/Math.abs(deltaE[j]);
					if (Double.isNaN(qNearDelta[i])) {
						qNearDelta[i] = 0;
					}
				}
				qNear[i] += (0.01/n+0.0005)*qNearDelta[i]; // Should I use some fixed value alpha instead of deltaH?
			}
			qNearPose = RobotJoints.getForwardKinematics(qNear);
			c++;

			if (Math.IEEEremainder(c, 1000) == 0) {
				// System.out.println("[DEBUG] "+"Cycle: "+c+", New Pose: " + Arrays.toString(qNearPose)+", QNearDelta = " +Arrays.toString(qNearDelta));
				n++;
			}
			if ( c == 10000){
				// qNear = getInverseKinematics(goalPose, qNear);
				// System.out.println(new Matrix(invJacobian).toString());

				break;
			}
		}
		for (int i = 0; i < 6; i++) {
			double twoPi = Math.PI*2;
			if (qNear[i] > twoPi) {
				qNear[i] = Math.IEEEremainder(qNear[i], twoPi); 
			}
			if (qNear[i] < -twoPi) {
				qNear[i] = -Math.IEEEremainder(qNear[i], twoPi); 
			}
			if ((qNear[i]-jointAngles[i]) < -Math.PI) {
				qNear[i] += twoPi; 
			} else if ((qNear[i]-jointAngles[i]) > Math.PI) {
				qNear[i] -= twoPi; 
			}
		}
		System.out.println(jacobian.toString());
		qNearPose = RobotJoints.getForwardKinematics(qNear);


		System.out.println("[DEBUG] "+"Time (ms): " + (System.currentTimeMillis()-time));

		System.out.println("[DEBUG] "+"Cycle: "+c+", New Pose: " + Arrays.toString(qNearPose));
		System.out.println("[DEBUG] "+"qNear: "+ Arrays.toString(qNear));
		System.out.println("[DEBUG] "+"qNearDelta: "+ Arrays.toString(qNearDelta));

		return qNear;
	}

	private static double[][] computeJacobian(double[] qNear, double deltaH){
		double[] pose = RobotJoints.getForwardKinematics(qNear);
		double[][] jacobian = new double[qNear.length][pose.length];
		double[] qNearTemp;
		for (int i = 0; i < qNear.length; i++) {
			qNearTemp = qNear.clone();
			qNearTemp[i] += deltaH;
			double[] deltaPose = RobotJoints.getForwardKinematics(qNearTemp);
			for (int j = 0; j < pose.length; j++) {
				jacobian[i][j] = ( pose[j] - deltaPose[j] ) / deltaH;
				if (Double.isNaN(jacobian[i][j])) {
					System.out.println(" >> [DEBUG] Problems with calc jacobian: " + (pose[j] - deltaPose[j]));
					return computeJacobian(qNear, (1+Math.random())*deltaH);
				}
			}
		}
		return jacobian;
	}

	/*
	 * Same as UR pose_trans(this, p2)
	 */
	public RobotPose pose_trans(RobotPose p2) {
		double[][] result = Matrix.multiply(getTransformMatrix(), p2.getTransformMatrix());
		return new RobotPose(result);
	}
	
	public static double[] pose_trans(double[] p1, double[] p2) {
		RobotPose pose1 = new RobotPose(p1);
		RobotPose pose2 = new RobotPose(p2);
		double[][] result = Matrix.multiply(pose1.getTransformMatrix(), pose2.getTransformMatrix());
		RobotPose xform = new RobotPose(result);
		return xform.toDoubleArray();
	}
	
	public double[] toDoubleArray() {
		double[] ret = new double[]{
				pose.get(0), pose.get(1), pose.get(2),
				pose.get(3), pose.get(4), pose.get(5)
		};
		return ret;
	}
	
	public String asString() {
		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits(4);
		String str = "p[ ";
		for (int i = 0; i < pose.size(); i++) {
			str += df.format(epsilon(pose.get(i)));
			if (i < pose.size() - 1) str += ", ";
		}
		str += " ]";
		return str;
	}
	
	private double epsilon(double comp) {
		if (Math.abs(comp) < .001) return 0;
		else return comp;
	}
	
	private static boolean epsilonEquals(double[] pose1, double[] pose2, double diffMM, double diffRad) {
		if (pose1.length != pose2.length) {
			System.out.println("[DEBUG] "+"Poses are not same length."+ Arrays.toString(pose1) +" and " + Arrays.toString(pose2));
			return false;
		}
		double diff = diffMM;
		for (int i = 0; i < pose1.length; i++) {
			if (i == 3) {
				diff = diffRad;
			}
			if ( Math.abs(pose1[i]-pose2[i]) > diff) {
				return false;
			}
		} 
		return true;
	}

	private boolean epsilonEquals(double[] pose2, double diffMM, double diffRad) {
		return epsilonEquals(this.toDoubleArray(), pose2, diffMM, diffRad);
	}

	public static double[] pose_inv(double[] pos) {
		return (new RobotPose(pos)).invert();
	}

	public double pose_dist(RobotPose p2) {
		return distance(getTranslationVector(), p2.getTranslationVector());
	}

	public static double distance(double[] pose1, double[] pose2) {
		return Math.sqrt( Math.pow((pose2[0]-pose1[0]), 2) + Math.pow((pose2[1]-pose1[1]), 2) + Math.pow((pose2[2]-pose1[2]), 2) );
	}
	public static void main(String[] args){
		// double[] qNear = {0.1,0,0.1,0,3.1415,0};
		// double[] poseDouble = {-0.2,-0.1,0.2,0,3.1415,-0};
		double[] qNear = new double[6];
		
		double[] poseDouble = new double[6];

		for (int i = 0; i < 6; i++) {
			qNear[i] = (Math.random()-0.5)*Math.PI*2;
			if (i>2) {
				poseDouble[i] = (Math.random()-0.5)*Math.PI*2;
			} else {
				poseDouble[i] = (Math.random()-0.5)*0.45;
			}
		}
		// double[] poseDouble = {-0.08525476798634873, -0.014094049060687319, -0.10748979264276443, 1.3224704893244592, -2.1888794886942096, 0.14333347979878622};
		// double[] qNear = {1.6985402600601442, -0.13686895333522314, 1.3324046627893469, 1.0047710669973375, 0.26421623295949476, 2.356353960453749};

		RobotPose pose = new RobotPose(poseDouble);
		double[] newQ = getInverseKinematicsJacob(pose, qNear.clone());
		System.out.println("Pose wanted: "+Arrays.toString(poseDouble));
		poseDouble = RobotJoints.getForwardKinematics(newQ);
		System.out.println("New Pose: "+Arrays.toString(poseDouble));

		System.out.println("\nQNear: "+Arrays.toString(qNear));
		System.out.println("New angles Rad: "+Arrays.toString(newQ));
		

		for (int i = 0; i < newQ.length; i++) {
			newQ[i] =  57.2957795 * newQ[i];
		}
		System.out.println("New angles Deg: "+Arrays.toString(newQ));
	}
}
